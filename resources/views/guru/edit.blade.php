@extends('master')

@section('content')
    
    <div class="container pt-4 bg-white">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit Guru</h1>
                <hr>
                <form action=" {{ route('gurus.update', ['guru' => $guru->id]) }} " method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="nik">NIK</label>
                        <input type="text" class="form-control @error ('nik') is-invalid @enderror" id="nik" name="nik" value=" {{old('nik') ?? $guru->nik }} ">
                        @error('nik')
                            <div class="text-danger"> {{ $message }} </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control @error('record') @enderror" id="nama" name="nama" value=" {{ old('nama') ?? $guru->nama }} ">
                        @error('nama')
                            <div class="text-danger"> {{ $message }} </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="laki-laki" value="L" {{ (old('jenis_kelamin') ?? $guru->jenis_kelamin) == 'L' ? 'checked' : '' }}>
                        <label class="form-check-label" for="laki-laki">Laki-laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="perempuan" value="L" {{ (old('jenis_kelamin') ?? $guru->jenis_kelamin) == 'L' ? 'checked' : '' }}>
                        <label class="form-check-label" for="perempuan">Perempuan</label>
                    </div>
                    @error('jenis_kelamin')
                        <div class="text-danger"> {{ $message }} </div>
                    @enderror

                    <div class="form-group">
                        <label for="bagian">Bagian</label>
                        <select class="form-control" name="bagian" id="bagian">
                            <option value="Frontend Developer" {{ (old('bagian') ?? $guru->bagian) == 'Frontend Developer' ? 'selected' : '' }} >
                                Frontend Developer
                            </option>
                            <option value="Backend Developer" {{ (old('bagian') ?? $guru->bagian) == 'Backend Developer' ? 'selected' : '' }} >
                                Backend Developer
                            </option>
                            <option value="Fullstack Developer" {{ (old('bagian') ?? $guru->bagian) == 'Fullstack Developer' ? 'selected' : '' }} >
                                Fullstack Developer
                            </option>
                            <option value="Digital Marketing" {{ (old('bagian') ?? $guru->bagian) == 'Digital Marketing' ? 'selected' : '' }} >
                                Digital Marketing
                            </option>
                            <option value="UI / UX" {{ (old('bagian') ?? $guru->bagian) == 'UI / UX' ? 'selected' : '' }} >
                                UI / UX
                            </option>
                        </select>
                        @error('bagian')
                            <div class="text-danger"> {{ $message }} </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" id="alamat" rows="3"> {{ old('alamat') ?? $guru->alamat }} </textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Update</button>
                </form>
            </div>
        </div>
    </div>

@endsection
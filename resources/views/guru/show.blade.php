@extends('master')

@section('content')
    
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="pt-3 d-flex justify-content-end align-items-center">
                    <h1 class="h2 mr-auto">Biodata {{$guru->nama}}</h1>
                    <a href=" {{route('gurus.edit', $guru->id)}} " class="btn btn-warning"> UBAH </a>

                    <form action=" {{route('gurus.destroy', $guru->id)}} " method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                    </form>
                </div>
                <hr>
                @if (session()->has('pesan'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('pesan') }}
                    </div>
                @endif
                <ul>
                    <li> Nik: {{ $guru->nik}} </li>
                    <li> Nama: {{ $guru->nama}} </li>
                    <li> Jenis Kelamin: {{ $guru->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-laki'}} </li>
                    <li> Bagian: {{ $guru->bagian}} </li>
                    <li> Alamat: {{ $guru->alamat == '' ? 'N/A' : $guru->alamat}} </li>
                </ul>
            </div>
        </div>
    </div>

@endsection
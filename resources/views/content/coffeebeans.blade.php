@extends('master-web')
@section('title', 'Coffee Beans | Kopi-Ku')

@section('content1')

<div class="container container-coffeebean">
    <h1>Coffee Beans</h1>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" data-toggle="modal" data-target="#product1-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product1-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Arabica Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" alt="" width="400px" height="250px">
                                <p>If you ever drink wine, then you will understand why this coffee is touted as the Merlot of coffee. It tastes sweet but is generally mild when taken by mouth. <br><br> A cup of Arabica coffee can be described as aromatic
                                    and rich in flavor. This is why, if you buy Arabica coffee at a coffee shop, you can see tasting notes on the packaging, for example: floral, fruity, orange, buttery, chocolate, caramel, and others.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Arabica Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" data-toggle="modal" data-target="#product2-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Robusta Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" alt="" width="400px" height="250px">
                                <p>Generally, Robusta is often described as a bitter or sharp coffee with a taste character like wood and rubber. <br><br> This bitter or bitter comes from the higher caffeine content in Robusta when compared to Arabica.
                                    If you are just looking for caffeine Robusta is the best choice!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Robusta Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" data-toggle="modal" data-target="#product3-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liberika Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" alt="" width="400px" height="250px">
                                <p>The aroma obtained from Liberika or excelsa coffee is very distinctive and easy to distinguish from Robusta or Arabica coffee. Sharp sting, with a thicker bitter taste. <br><br> Typically, Liberika coffee is mixed with
                                    milk to mask the sharp aroma and bitter taste of the coffee. Also often used as a mixture for Robusta coffee to add extra coffee aroma.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Liberika Beans</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" data-toggle="modal" data-target="#product4-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product4-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liberika Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" alt="" width="400px" height="250px">
                                <p>The aroma obtained from Liberika or excelsa coffee is very distinctive and easy to distinguish from Robusta or Arabica coffee. Sharp sting, with a thicker bitter taste. <br><br> Typically, Liberika coffee is mixed with
                                    milk to mask the sharp aroma and bitter taste of the coffee. Also often used as a mixture for Robusta coffee to add extra coffee aroma.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Liberika Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" data-toggle="modal" data-target="#product5-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product5-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Arabica Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" alt="" width="400px" height="250px">
                                <p>If you ever drink wine, then you will understand why this coffee is touted as the Merlot of coffee. It tastes sweet but is generally mild when taken by mouth. <br><br> A cup of Arabica coffee can be described as aromatic
                                    and rich in flavor. This is why, if you buy Arabica coffee at a coffee shop, you can see tasting notes on the packaging, for example: floral, fruity, orange, buttery, chocolate, caramel, and others.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Arabica Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" data-toggle="modal" data-target="#product6-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product6-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Robusta Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" alt="" width="400px" height="250px">
                                <p>Generally, Robusta is often described as a bitter or sharp coffee with a taste character like wood and rubber. <br><br> This bitter or bitter comes from the higher caffeine content in Robusta when compared to Arabica.
                                    If you are just looking for caffeine Robusta is the best choice!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Robusta Beans</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" data-toggle="modal" data-target="#product7-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product7-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Robusta Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://1.bp.blogspot.com/-saO3Umfki-Q/Vk3img7XcYI/AAAAAAAAAEM/9Ha8Cj_rTUA/s1600/coffee-photography-hd-wallpaper-1920x1200-28197.jpg" alt="" width="400px" height="250px">
                                <p>Generally, Robusta is often described as a bitter or sharp coffee with a taste character like wood and rubber. <br><br> This bitter or bitter comes from the higher caffeine content in Robusta when compared to Arabica.
                                    If you are just looking for caffeine Robusta is the best choice!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Robusta Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" data-toggle="modal" data-target="#product8-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product8-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liberika Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://images.unsplash.com/photo-1447933601403-0c6688de566e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=956&q=80" alt="" width="400px" height="250px">
                                <p>The aroma obtained from Liberika or excelsa coffee is very distinctive and easy to distinguish from Robusta or Arabica coffee. Sharp sting, with a thicker bitter taste. <br><br> Typically, Liberika coffee is mixed with
                                    milk to mask the sharp aroma and bitter taste of the coffee. Also often used as a mixture for Robusta coffee to add extra coffee aroma.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Liberika Beans</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 p-1 p-md-4 px-5">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" data-toggle="modal" data-target="#product9-bean" height="200px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product9-bean" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeebean">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Arabica Beans</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <img src="https://lh6.googleusercontent.com/proxy/zSahua9tjQqgrATntIQdC6qkQyZv5qU1NNqERzOcWSpza2p0qPYWkjLA9b_LbisvKZLuwPxQdkwZ4qgGxTVlR1vAizA3oe8BmP_grmyEOoZzo3y0i1_eVbGE4qkolM4=s0-d" alt="" width="400px" height="250px">
                                <p>If you ever drink wine, then you will understand why this coffee is touted as the Merlot of coffee. It tastes sweet but is generally mild when taken by mouth. <br><br> A cup of Arabica coffee can be described as aromatic
                                    and rich in flavor. This is why, if you buy Arabica coffee at a coffee shop, you can see tasting notes on the packaging, for example: floral, fruity, orange, buttery, chocolate, caramel, and others.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Arabica Beans</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
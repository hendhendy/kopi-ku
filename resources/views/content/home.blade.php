@extends('master-web')
@section('title', 'HOME | Kopi-Ku')

@section ('content1')
<div class="caro-home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner slider-home">
            <div class="carousel-item active">
                <img src="https://images.unsplash.com/photo-1518832553480-cd0e625ed3e6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" class="d-block w-100 img-fluid" style="height: 50rem;" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1583414169668-f65860b4dece?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" class="d-block w-100 img-fluid" style="height: 50rem;" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1595244333063-a394c9cdd63f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80" class="d-block w-100 img-fluid" style="height: 50rem;" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1569161238483-4c6375c59d68?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" class="d-block w-100 img-fluid" style="height: 50rem;" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div> 
</div>
@endsection

@section('content2')
<div class="jumbotron jumbotron-fluid" id="middle-home">
    <div class="container container-jumbo">
        <div class="row">
            <div class="col-6 col-md-6 col-lg-6">
                <img src="https://images.unsplash.com/photo-1551148321-105bb1f3aec6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80" width="100%" alt="">
            </div>
            <div class="col-6 col-md-6 col-lg-6">
                <h1>Moving Forward</h1>
                <p>Obsessed with reinventing the way people drink coffee and the experience around coffee, we designed modern contemporary store environments; providing a social canvas with comfortable settings and thoughtful features including co-working
                    spaces, functional meeting rooms and manual brew bar offering select range of single origin coffees handcrafted with different brewing methods. <mark>Kopi-Ku!</mark> inspires an appetite for ‘living life more happy with <mark>Kopi-Ku!</mark>’
                    A place to meet with friends and family and experience memorable and engaging moments around great coffee, delicious food and mouthwatering beverages.</p>
            </div>
        </div>
    </div>
</div>    
@endsection
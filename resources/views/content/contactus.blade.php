@extends('master-web')
@section('title', 'Contact Us | Kopi-Ku')

@section('content1')
    <div class="container container-contactus-page">
        <h1><span style="background-color: whitesmoke;">24 hour Services</span></h1><br><br>
        <p style="font-size: x-large;"><mark>Kopi-Ku</mark> melayani pelayanan secara 24jam kepada setiap customer kami.
            <br><br>&nbsp;Silahkan Hubungi Kami untuk info lebih lengkap :
        </p> <br><br>
        <div class="row">

            <!-- Button trigger modal contact us -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Contact Us
            </button>

            <!-- Modal contact us -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Kontak Kami</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4 col-md-4 col-lg-4">
                                        <label for="">Alamat :</label><br>
                                        <label for="">HP/Whatsapp :</label>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-6">
                                        <label for="">Jl.Menceng Raya No.50</label><br>
                                        <label for="">0812-3456-789</label>
                                    </div>
                                    <div class="col-2 col-md-2 col-lg-2">
                                        <!-- kosong -->
                                    </div>
                                </div><br>
                                <label> Jakarta Barat, Kalideres. 10650</label>
                            </div>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.541618950893!2d106.69866081497254!3d-6.192031462389417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f97c1edc3475%3A0xc4a1607fe3e5804f!2sEdutech!5e0!3m2!1sid!2sid!4v1594894543082!5m2!1sid!2sid"
                                width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Dropdown code -->
            <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true"> DAFTAR MEMBER <span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#" data-toggle="modal" data-target="#exampleModal1" class="btn btn-danger">Aku Pelajar!</a>
                    </li>
                    <li><a href="#" data-toggle="modal" data-target="#exampleModal2" class="btn btn-warning">Aku Guru!</a>
                    </li>
                </ul>
            </div>

            {{--
            <!-- Button trigger modal form -->
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal1">
                Aku Pelajar!
            </button> --}}


            <!-- Modal form -->
            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">DATA PELAJAR</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" id="modal-form">
                                    <form action="{{ route('kopiku.store') }} " method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nik">NIK</label>
                                            <input type="text" class="form-control @error('nik') is-invalid @enderror"
                                                id="nik" name="nik" value="{{ old('nik') }}">
                                            @error('nik')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="nama">Nama Lengkap</label>
                                            <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                name="nama" id="nama" value="{{ old('nama') }} ">
                                            @error('nama')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="laki_laki"
                                                value="L" {{ old('jenis_kelamin') == 'L' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="laki_laki">Laki-laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="perempuan"
                                                value="P" {{ old('jenis_kelamin') == 'P' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="perempuan">Perempuan</label>
                                        </div>
                                        @error('jenis_kelamin')
                                            <div class="text-danger"> {{ $message }} </div>
                                        @enderror
                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label for="bagian">Bagian</label>
                                            <select class="form-control" name="bagian" id="bagian">
                                                <option value="Frontend Developer"
                                                    {{ old('bagian') == 'Frontend Developer' ? 'selected' : '' }}>
                                                    Frontend Developer
                                                </option>
                                                <option value="Backend Developer"
                                                    {{ old('bagian') == 'Backend Developer' ? 'selected' : '' }}>
                                                    Backend Developer
                                                </option>
                                                <option value="Fullstack Developer"
                                                    {{ old('bagian') == 'Fullstack Developer' ? 'selected' : '' }}>
                                                    Fullstack Developer
                                                </option>
                                                <option value="Digital Marketing"
                                                    {{ old('bagian') == 'Digital Marketing' ? 'selected' : '' }}>
                                                    Digital Marketing
                                                </option>
                                                <option value="UI / UX" {{ old('bagian') == 'UI / UX' ? 'selected' : '' }}>
                                                    UI / UX
                                                </option>
                                            </select>
                                            @error('bagian')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="alamat">Alamat</label> <br>
                                            <textarea name="alamat" id="alamat" rows="3"> {{ old('alamat') }} </textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2">SIMPAN</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--
            <!-- Button trigger modal form -->
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal1">
                Aku Guru!
            </button> --}}

            <!-- Modal form -->
            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">DATA GURU</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" id="modal-form">
                                    <form action="{{ route('kopiku.store') }} " method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nik">NIK</label>
                                            <input type="text" class="form-control @error('nik') is-invalid @enderror"
                                                id="nik" name="nik" value="{{ old('nik') }}">
                                            @error('nik')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="nama">Nama Lengkap</label>
                                            <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                name="nama" id="nama" value="{{ old('nama') }} ">
                                            @error('nama')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="laki_laki"
                                                value="L" {{ old('jenis_kelamin') == 'L' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="laki_laki">Laki-laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="perempuan"
                                                value="P" {{ old('jenis_kelamin') == 'P' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="perempuan">Perempuan</label>
                                        </div>
                                        @error('jenis_kelamin')
                                            <div class="text-danger"> {{ $message }} </div>
                                        @enderror
                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label for="bagian">Bagian</label>
                                            <select class="form-control" name="bagian" id="bagian">
                                                <option value="Frontend Developer"
                                                    {{ old('bagian') == 'Frontend Developer' ? 'selected' : '' }}>
                                                    Frontend Developer
                                                </option>
                                                <option value="Backend Developer"
                                                    {{ old('bagian') == 'Backend Developer' ? 'selected' : '' }}>
                                                    Backend Developer
                                                </option>
                                                <option value="Fullstack Developer"
                                                    {{ old('bagian') == 'Fullstack Developer' ? 'selected' : '' }}>
                                                    Fullstack Developer
                                                </option>
                                                <option value="Digital Marketing"
                                                    {{ old('bagian') == 'Digital Marketing' ? 'selected' : '' }}>
                                                    Digital Marketing
                                                </option>
                                                <option value="UI / UX" {{ old('bagian') == 'UI / UX' ? 'selected' : '' }}>
                                                    UI / UX
                                                </option>
                                            </select>
                                            @error('bagian')
                                                <div class="text-danger"> {{ $message }} </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="alamat">Alamat</label> <br>
                                            <textarea name="alamat" id="alamat" rows="3"> {{ old('alamat') }} </textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2">SIMPAN</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

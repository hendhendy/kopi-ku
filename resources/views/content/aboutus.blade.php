@extends('master-web')
@section('title', 'About Us | Kopi-Ku')

@section('content1')

<div class="container container-aboutus-page">
    <div data-aos="zoom-out" data-aos-duration="2000">
    <h1 style="padding-top: 3%;">Story</h1> <br>
        
    <p> The <mark>Kopi-Ku</mark> story began with a deep passion to provide Indonesians with the best coffee and lifestyle experiences. In March 2015, our founders opened the first <mark>Kopi-Ku</mark> store in Tangerang, East of Jakarta with aspirations
        of becoming Indonesia’s number one local coffee chain, serving wide range of specialty coffees made with freshly ground Arabica beans sourced from coffee regions in Indonesia and around the world. <br><br> Although passionate about sharing
        our love for life and coffee, we had to search deep within our hearts to discover that we had fallen short of expressing our identity and the experience we envisioned sharing with our guests. And so, in December 2018 the Maxx Coffee revolution
        was set in motion with our ‘refresh’ brand transformation, launching our first ever flagship store in Kemang, South Jakarta. <br><br> Obsessed with reinventing the way people drink coffee and the experience around coffee, we designed modern
        contemporary store environments; providing a social canvas with comfortable settings and thoughtful features including co-working spaces, functional meeting rooms and manual brew bar offering select range of single origin coffees handcrafted
        with different brewing methods. Our beverage innovation led us to pioneer nitro cold brew coffee and tea which has proven to be a popular choice for its rich, creamy taste and texture. And creative signature foods ranging from artisanal sandwiches
        like NZ ‘coffee rubbed’ tenderloin to baked favorites such as the blueberry Scoffin. <br><br> Our culture is built on nurturing our people to embody the values of our brand and express our promise through honest and warm guest engagement,
        with superior delivery of quality and service. Valuing the importance of our people, we launched the Maxx Coffee Academy to perfect our barista’s skill set and craft, recognizing that harmony between barista and espresso machine is the key
        to that perfect cup of coffee. Committed to quality and sustainability, we source premium and specialty grade coffee beans in collaboration with local coffee farmers and imported beans from accredited coffee farmers abroad. We believe that
        cultivating the best of these three elements; people, technology and coffee, is the winning formula for success. <br><br>
        <mark>Kopi-Ku</mark> inspires an appetite for ‘living life to the Maxx!’ A place to meet with friends and family and experience memorable and engaging moments around great coffee, delicious food and mouthwatering beverages. We hope that everyone
        will come together and share their special moments with us. We are proud to be Indonesian and proud to serve guests in 22 cities throughout Indonesia. It is also our privilege to extend our passion beyond our borders and enter new markets.
        And we are still growing! </p>
    </div>
</div>
@endsection
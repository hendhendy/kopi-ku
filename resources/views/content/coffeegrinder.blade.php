@extends('master-web')
@section('title', 'Coffee Grinder | Kopi-Ku')

@section('content1')
<div class="container-fluid container-coffeegrinder">
    <h1>Coffee Grinder</h1>
    <div class="row mb-4">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product1-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product1-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>


    <div class="row mb-4">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product1-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product1-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>


    <div class="row mb-4">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product1-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product1-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" data-toggle="modal" data-target="#product2-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product2-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
                                        <li data-target="#carouselExampleIndicators3" data-slide-to="4"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_3f4be472-2fb8-4c83-bcfe-96458b04921f_995_995.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_626d3023-bebd-4a63-afa3-bfa908c4d403_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_75249b5c-d9f7-4904-b114-71ef71fddd76_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_9c4849af-3fd0-4f04-965f-ce4d83c46edc_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/7/655681/655681_416d0924-4765-4ab6-9b15-c134f0c47361_1080_1080.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Breville the Barista Pro akan membuat kopi anda semakin nikmat , karena dengan grinder ini tingkat kehalusan gilingan bisa anda setel dengan mudah , dan konsistensi dari gilingan sangat rata. <br><br> Setting manual akan memudahkan
                                    anda mengatur seberapa halus hasil gilingan yang kamu inginkan. <br> Gilingan ini mempunyai safety lock yang sangat bagus , jika semua tidak pada posisi yang benar maka mesin ini tidak akan beroperasi. <br><br>                                        Memakai sistem dual burrs sehingga mesin ini tidak akan cepat panas , dan ada cup selector yang otomatis bisa berhenti jika gilingan sudah tercapai bisa menampung 120 gr coffee cukup untuk 12 cups.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Breville the Barista Pro Coffee Grinder</h5>
                </div>
            </div>
        </div>
        <div class=" col-12 col-md-6 col-lg-3">
            <div class="card">
                <!-- Button trigger modal contact us -->
                <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" data-toggle="modal" data-target="#product3-grinder" height="300px"></img>

                <!-- Modal contact us -->
                <div class="modal fade" id="product3-grinder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content modal-coffeegrinder">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body modal-konten">
                                <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                        <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_6f4190ee-35cd-407f-a90e-5e312f16b35e_736_861.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_5c9ab245-95cb-4a93-bcd2-84e36fb7804e_800_533.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_a78b21cc-b296-43f0-8907-93d064f20897_650_865.jpg.webp" class="img-fluid" height="400px" alt="...">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2018/3/21/0/0_c6f0de64-9363-45d8-8d8d-bc74d4f0231e_1000_300.jpg" class="img-fluid" height="400px" alt="...">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <p>Latina T-60 Libra coffee grinder, adalah pilihan alternatif dan LATINA 600N, sama2 dilengkapi dengan Flat burrs 60mm, model pisau vertical dan model mesin jahit (T). <br><br> Merupakan produk baru 2019 yang sudah dimutakhirkan,
                                    dengan sekring anti putus, swith 2 tahap, Kontinue maupun On demand. dengan ukuran yang sedikit lebih kecil, tetapi tetap bertenaga dan mengiling dengan konsistensi yang baik. <br><br> Mesin LATINA T60-Libra ini
                                    lebih desain untuk single origin, tetapi juga bisa dipakai untuk mesin espresso, dengan 10 setting kehalusan (1-5) dengan skala 0,5 per geseran. <br><br> Untuk mencapai kualitas gilingan yang sempurna buat espresso,
                                    kadang perlu menggantung sedikit di ditengah skala putar.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body card-body-grinder">
                    <h5 class="card-title">Latina T-60 Libra Coffee Grinder</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
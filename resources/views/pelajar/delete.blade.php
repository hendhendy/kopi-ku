@extends('master')

@section('content')
    
<form action=" {{route('pelajars.destroy', $pelajar->id)}} " method="POST">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger ml-3">Hapus</button>
    </form>

<hr>
@if (session()->has('pesan'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('pesan') }}
    </div>
@endif

@endsection
@extends('master')

@section('content')

    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="py-4">
                    <h1>Tabel Data Pelajar</h1>
                    <a href=" {{ route('pelajars.create') }} " class="btn btn-primary"> Tambah Pelajar </a>
                </div>

                @if (session() -> has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nik</th>
                            <th>Nama</th>
                            <Th>Jenis Kelamin</Th>
                            <th>Bagian</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pelajar as $pelajarz)
                            <tr>

                            </tr>
                            <th>{{ $loop->iteration }}</th>
                            {{-- Method 1 (works!!!) --}}
                            <td><a href="{{ route('pelajars.show', ['pelajar' => $pelajarz->id]) }}"> {{ $pelajarz->nik }} </a>
                            </td>
                            {{-- Method 2 (ga works) --}}
                            {{-- <td><a href=" {{ url('/pelajar/', $pelajarz->id) }}">
                                    {{ $pelajarz->nik }} </a></td> --}}
                            <td>{{ $pelajarz->nama }}</td>
                            <td>{{ $pelajarz->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-Laki' }}</td>
                            <td>{{ $pelajarz->bagian }}</td>
                            <td>{{ $pelajarz->alamat == '' ? 'N/A' : $pelajarz->alamat }}</td>
                            <td>
                                <div class="row">
                                    <button class="btn btn-warning"><a style="color: black" href="{{route('pelajars.edit', $pelajarz->id)}}">Edit</a></button>
                                    <form action=" {{route('pelajars.destroy', $pelajarz->id)}} " method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                                        </form>
                                </div>
                            </td>
                        @empty
                            <td colspan="8" class="text-center">Data Kosong</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

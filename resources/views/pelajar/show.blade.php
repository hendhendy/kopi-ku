@extends('master')

@section('content')
    
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="pt-3 d-flex justify-content-end align-items-center">
                    <h1 class="h2 mr-auto">Biodata {{$pelajar->nama}}</h1>
                    <a href=" {{route('pelajars.edit', $pelajar->id)}} " class="btn btn-warning"> UBAH </a>

                    <form action=" {{route('pelajars.destroy', $pelajar->id)}} " method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                    </form>
                </div>
                <hr>
                @if (session()->has('pesan'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('pesan') }}
                    </div>
                @endif
                <ul>
                    <li> Nik: {{ $pelajar->nik}} </li>
                    <li> Nama: {{ $pelajar->nama}} </li>
                    <li> Jenis Kelamin: {{ $pelajar->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-laki'}} </li>
                    <li> Bagian: {{ $pelajar->bagian}} </li>
                    <li> Alamat: {{ $pelajar->alamat == '' ? 'N/A' : $pelajar->alamat}} </li>
                </ul>
            </div>
        </div>
    </div>

@endsection
@extends('master')

@section('content')
    
<div class="container bg-white">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center"> DATA PELAJAR </h1>
            <form action="{{ route('kopiku.store') }} " method="POST">
                @csrf
                <div class="form-group">
                    <label for="nik">NIK</label>
                    <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" 
                    value="{{ old('nik') }}">
                    @error('nik')
                        <div class="text-danger">  {{ $message }} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama"
                    value="{{ old('nama') }} ">
                    @error('nama')
                        <div class="text-danger"> {{ $message }} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Jenis Kelamin</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="laki_laki" value="L"
                    {{ old('jenis_kelamin')=='L' ? 'checked': '' }} >
                    <label class="form-check-label" for="laki_laki">Laki-laki</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="perempuan" value="P"
                    {{ old('jenis_kelamin')=='P' ? 'checked': '' }} >
                    <label class="form-check-label" for="perempuan">Perempuan</label>
                </div>
                @error('jenis_kelamin')
                    <div class="text-danger"> {{ $message }} </div>
                @enderror

                <div class="form-group">
                    <label for="bagian">Bagian</label>
                    <select class="form-control" name="bagian" id="bagian">
                        <option value="Frontend Developer" {{ old('bagian')=='Frontend Developer' ? 'selected': '' }} >
                            Frontend Developer
                        </option>
                        <option value="Backend Developer" {{ old('bagian')=='Backend Developer' ? 'selected': '' }} >
                            Backend Developer
                        </option>
                        <option value="Fullstack Developer" {{ old('bagian')=='Fullstack Developer' ? 'selected': '' }} >
                            Fullstack Developer
                        </option>
                        <option value="Digital Marketing" {{ old('bagian')=='Digital Marketing' ? 'selected': '' }} >
                            Digital Marketing
                        </option>
                        <option value="UI / UX" {{ old('bagian')=='UI / UX' ? 'selected': '' }} >
                            UI / UX
                        </option>
                    </select>
                    @error('bagian')
                        <div class="text-danger"> {{ $message }} </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" rows="3"> {{ old('alamat') }} </textarea>
                </div>

                <button type="submit" class="btn btn-primary mb-2">SIMPAN</button>
                
            </form>
        </div>
    </div>
</div>

@endsection

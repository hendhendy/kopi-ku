<body class="hold-transition sidebar-mini layout-fixed">
<!-- /.content-wrapper -->
  <footer class="main-footer" style="">
    <strong>Copyright &copy; 2014-2019 <a href="#">AdminLTE</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0
    </div>
  </footer>
</body>
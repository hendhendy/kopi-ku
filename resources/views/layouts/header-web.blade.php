<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="line-height: 60px;" id="nav-bar-page">
    <div class="container container-navbar">
        <a class="navbar-brand" href="/kopiku/halaman-utama"><img src="/img/pngwing.com.png" class="img-fluid" style="height: 5rem; width: 10rem;" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto navbar-atas">
                <li class="nav-item active">
                    <a class="nav-link" href="/kopiku/halaman-utama" data-transition="slide">Home <span
                            class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Products
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item nav-drop-1" href="/kopiku/coffeegrinder">Coffee Grinder</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item nav-drop-2" href="/kopiku/coffeebeans">Coffee Beans</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/kopiku/contactus" id="contactus">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/kopiku/aboutus" data-transition="slide" id="aboutus">About Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
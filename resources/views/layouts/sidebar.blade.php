<body class="hold-transition sidebar-mini layout-fixed">
<!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    <img src="https://ecs7.tokopedia.net/img/cache/700/VqbcmM/2020/10/15/c1c6e6df-3f6e-4ffe-b17c-fb9bcc69f6f4.jpg" alt="AdminKopiKu Logo" class="brand-image img-circle elevation-4"
                        style="opacity: 0.8">
                    <span class="brand-text font-weight-light">Admin Kopi-Ku!</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="https://w7.pngwing.com/pngs/164/34/png-transparent-computer-icons-businessperson-user-icon-man-people-logo-black.png" class="img-fluid img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">Hendy Hanan</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                            <li class="nav-item has-treeview menu-open">
                                <a href="#" class="nav-link active">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('pelajars.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Pelajar</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('gurus.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Guru</p>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a href="./index3.html" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Materi</p>
                                        </a>
                                    </li> --}}
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
</body>
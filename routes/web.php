<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content.home');
});


// Route Pelajar //

// Route::get('/', 'PelajarController@index')->name('pelajar.index');
// Route::get('/pelajar/create', 'PelajarController@create')->name('pelajar.create');
// Route::post('/pelajar', 'PelajarController@store')->name('pelajar.store');
// Route::get('/pelajar/{pelajar}', 'PelajarController@show')->name('pelajar.show');
// Route::get('/pelajar/{pelajar}/edit', 'PelajarController@edit')->name('pelajar.edit');
// Route::put('/pelajar/{pelajar}', 'PelajarController@update')->name('pelajar.update');
// Route::delete('/pelajar/{pelajar}', 'PelajarController@destroy')->name('pelajar.destroy');

Route::resource('pelajars', 'PelajarController')->middleware('auth');

// Route guru //

// Route::get('/guru', 'GuruController@index')->name('guru.index');
// Route::get('/guru/create', 'GuruController@create')->name('guru.create');
// Route::post('/guru', 'GuruController@store')->name('guru.store');
// Route::get('/guru/{guru}', 'GuruController@show')->name('guru.show');
// Route::get('/guru/{guru}/edit', 'GuruController@edit')->name('guru.edit');
// Route::put('/guru/{guru}', 'GuruController@update')->name('guru.update');
// Route::delete('/guru/{guru}', 'GuruController@destroy')->name('guru.destroy');

Route::resource('gurus', 'GuruController')->middleware('auth');

// Route web //

Route::get('/kopiku/create', 'PelajarController@createweb')->name('kopiku.create');
Route::post('/kopiku', 'PelajarController@storeweb')->name('kopiku.store');

Route::prefix('/kopiku')->group(function(){
    Route::get('/halaman-utama', function () {
        return view('content.home');
    });
    Route::get('/contactus', function () {
        return view('content.contactus');
    });
    Route::get('/aboutus', function () {
        return view('content.aboutus');
    });
    Route::get('/coffeegrinder', function () {
        return view('content.coffeegrinder');
    });
    Route::get('/coffeebeans', function () {
        return view('content.coffeebeans');
    });
});


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');


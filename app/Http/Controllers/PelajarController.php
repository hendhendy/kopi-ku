<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelajar;

class PelajarController extends Controller
{
    public function index()
    {
        // return "Tabel Pendataan Pelajar";
        $pelajar = Pelajar::all();
        return view('pelajar.index',['pelajar' => $pelajar]);
    }

    // public function show($pelajar)
    // {
    //     $result = Pelajar::findorfail($pelajar);
    //     return view('pelajar.show',['pelajar' => $result]);
    // }

    // Method Binding //
    public function show(Pelajar $pelajar)
    {
        return view('pelajar.show', ['pelajar' => $pelajar]);
    }

    public function create()
    {
        return view('pelajar.create');
    }

    public function createweb()
    {
        return view('kopiku.create');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nik' => 'required|size:8|unique:pelajars',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'bagian' => 'required',
            'alamat' => '',
        ]);
        
        // Method 1 //
        // $pelajar = new Pelajar();
        // $pelajar->nik = $validateData['nik'];
        // $pelajar->nama = $validateData['nama'];
        // $pelajar->jenis_kelamin = $validateData['jenis_kelamin'];
        // $pelajar->bagian = $validateData['bagian'];
        // $pelajar->alamat = $validateData['alamat'];
        // $pelajar->save();
        // return "Data Berhasil Disimpan";

        // Method Mass Assignment //
        Pelajar::create($validateData);
        $request->session()->flash('pesan', "Data {$validateData['nama']} berhasil di simpan ");
        return redirect()->route('pelajars.index');   
    }

    public function storeweb(Request $request)
    {
        $validateData = $request->validate([
            'nik' => 'required|size:8|unique:pelajars',
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'bagian' => 'required',
            'alamat' => '',
        ]);

        // Method Mass Assignment //
        Pelajar::create($validateData);
        $request->session()->flash('pesan', "Data {$validateData['nama']} berhasil di simpan ");
        return redirect('/kopiku/contactus');   
    }


    public function edit(Pelajar $pelajar)
    {
        return view('pelajar.edit', ['pelajar' => $pelajar]);
    }

    public function update(Request $request, Pelajar $pelajar)
    {
        $validateData = $request->validate([
            'nik' => 'required|size:8|unique:pelajars,nik,'.$pelajar->id,
            'nama' => 'required|min:3|max:50',
            'jenis_kelamin' => 'required|in:P,L',
            'bagian' => 'required',
            'alamat' => '',
        ]);
        $pelajar->update($validateData);
        return redirect()->route('pelajars.show',['pelajar' => $pelajar->id])->with('pesan', "Update Data {$validateData['nama']} Berhasil");
    }

    public function destroy(Pelajar $pelajar)
    {
        $pelajar->delete();
        return redirect()->route('pelajars.index')->with('pesan', "Hapus Data $pelajar->nama BERHASIL");
    }
}
